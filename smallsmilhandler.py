#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    # Clase para manejar smill

    def __init__(self):

        # Constructor. Inicializamos las variables

        self.lista_etiquetas = []
        self.dic = {'root-layout': ['width', 'height', 'background-color'],
                    'region': ['id', 'top', 'bottom', 'left', 'right'],
                    'img': ['src', 'region', 'begin', 'dur'],
                    'audio': ['src', 'begin', 'dur'],
                    'textstream': ['src', 'region']}

    def startElement(self, valor, atrib):

        # Método que se llama para alamacenar las etiquetas,
        # los atributos y su contenido

        if valor in self.dic:
            dicc = {}
            for item in self.dic[valor]:
                dicc[item] = atrib.get(item, "")
            diccnombre = {valor: dicc}
            self.lista_etiquetas.append(diccnombre)

    def get_tags(self):

        # Método que devuelve las etiquetas,
        # los atributos y su contenido

        return self.lista_etiquetas


if __name__ == "__main__":

    # Programa principal

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
