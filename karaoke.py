import sys
import json
from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
from urllib.request import urlretrieve, urlcleanup


class KaraokeLocal(SmallSMILHandler):

    def __init__(self, fichero):

        # Parseará el fichero y obtendrá etiquetas

        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(fichero))
        self.lista_etiquetas = cHandler.get_tags()

    def __str__(self):
        fila = ""
        for dic in self.lista_etiquetas:
            llave = list(dic.keys())[0]
            fila = fila + llave
            for elemento in dic[llave]:
                valor = dic[llave][elemento]
                if valor:
                    valor = dic[llave][elemento]
                    linea = '\t' + elemento + '=' + '"' + valor + '"'
                    fila = fila + linea
            fila = fila + '\n'
        print(fila)

    def to_json(self, fichero, nombre=""):
        lista_etiquetas_json = json.dumps(self.lista_etiquetas)
        if not nombre:
            nombre = fichero.split('.')[0] + '.json'
        with open(nombre, 'w') as fichero_json:
            json.dump(lista_etiquetas_json, fichero_json, sort_keys=True, indent=4)

    def do_local(self):
        for dic in self.lista_etiquetas:
            llave = list(dic.keys())[0]
            for elemento in dic[llave]:
                valor = dic[llave][elemento]
                if valor:
                    if (elemento == "src") and (valor != "cancion.ogg"):
                        URL = valor
                        filename = URL[URL.rfind("/") + 1:]
                        data = urlretrieve(URL, filename)
                        urlcleanup()
                        dic[llave][elemento] = data[0]


if __name__ == "__main__":
    try:
        fichero = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 karaoke.py file.smil.")
    try:
        obj_karaokelocal = KaraokeLocal(fichero)
    except IOError:
        sys.exit("Usage: python3 karaoke.py file.smil.")
    obj_karaokelocal.__str__()
    obj_karaokelocal.do_local()
    obj_karaokelocal.to_json(fichero, 'local.json')
    obj_karaokelocal.__str__()
